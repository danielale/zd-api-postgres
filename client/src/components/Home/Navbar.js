import React, { Component } from "react";
import { NavItem, NavLink, NavbarNav, Fa, DropdownToggle, Dropdown, DropdownItem, DropdownMenu } from "mdbreact";

class Navbar extends Component {
	render() {
		return (
			<nav className="navbar navbar-expand-lg navbar-dark primary-color fixed-top">
				<div className="container">
					<a className="navbar-brand" href="/">
						ZipDance
					</a>

					<button
						className="navbar-toggler"
						type="button"
						data-toggle="collapse"
						data-target="#basicExampleNav"
						aria-controls="basicExampleNav"
						aria-expanded="false"
						aria-label="Toggle navigation"
					>
						<span className="navbar-toggler-icon" />
					</button>

					<div className="collapse navbar-collapse" id="basicExampleNav">
						<ul className="navbar-nav mr-auto">
							<li className="nav-item active">
								<a className="nav-link" href="/">
									Home
									<span className="sr-only">(current)</span>
								</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" href="/competition">
									Competition
								</a>
							</li>

							<li className="nav-item dropdown">
								<Dropdown>
									<DropdownToggle nav caret>
										Admin
									</DropdownToggle>
									<DropdownMenu className="dropdown-default">
										<DropdownItem href="/new">New Competition</DropdownItem>
										<DropdownItem href="#!">Manage Heat</DropdownItem>
										<DropdownItem href="#!">Manage Scores</DropdownItem>
									</DropdownMenu>
								</Dropdown>
							</li>
						</ul>

						<NavbarNav right>
							<NavItem>
								<NavLink className="waves-effect waves-light" to="#!">
									<Fa icon="twitter" />
								</NavLink>
							</NavItem>
							<NavItem>
								<NavLink className="waves-effect waves-light" to="#!">
									<Fa icon="google-plus" />
								</NavLink>
							</NavItem>
							<NavItem>
								<Dropdown>
									<DropdownToggle nav caret>
										<Fa icon="user" />
									</DropdownToggle>
									<DropdownMenu className="dropdown-default" right>
										<DropdownItem href="#!">Settings</DropdownItem>
										<DropdownItem href="#!">Profile</DropdownItem>
										<DropdownItem href="#!">Logout</DropdownItem>
									</DropdownMenu>
								</Dropdown>
							</NavItem>
						</NavbarNav>
					</div>
				</div>
			</nav>
		);
	}
}

export default Navbar;
