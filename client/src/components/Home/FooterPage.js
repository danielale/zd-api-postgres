import React from "react";
import { Container, Footer } from "mdbreact";

class FooterPage extends React.Component {
	render() {
		return (
			<div className="text-center py-3 mt-4 page-footer font-small blue pt-4 zp-footer">
				<Container fluid>
					&copy; {new Date().getFullYear()} Copyright, <a href="https://www.google.com"> ZipDance </a>
				</Container>
			</div>
		);
	}
}

export default FooterPage;
