import React from "react";
import { Component } from "react";
import { Link } from "react-router-dom";

class Landing extends Component {
	render() {
		return (
			<div className="landing intro view">
				<div className="mask rgba-black-strong">
					<div className="container-fluid d-flex align-items-center justify-content-center h-100">
						<div className="row d-flex justify-content-center text-center">
							<div className="col-md-10">
								<h2 className="display-4 font-weight-bold white-text pt-5 mb-2">ZipDance</h2>

								<hr className="hr-light" />

								<h4 className="white-text my-4">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti consequuntur.
								</h4>
								<button type="button" className="btn btn-outline-white">
									Explore<i className="fa fa-book ml-2" />
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Landing;
