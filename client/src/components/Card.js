import React from "react";

export default (props) => {
	return (
		<div className="card">
			<div className="card-image">
				<img src={props.image} alt={props.title} width="width:100%" />
			</div>
			<div className="card-body text-center">
				<h4>
					<b>{props.title}</b>
				</h4>
				<p class="lead ">{props.body}</p>
				<span>
					{props.start_date} -- {props.end_date}
				</span>
			</div>
		</div>
	);
};
