import React, { Component } from "react";
import { MDBContainer, MDBRow, MDBCol, MDBInput } from "mdbreact";
// import FileUpload from "./FileUpload";
import DatePickerInput from "./DatePickerInput";
import axios from "axios";
import DatePicker from "react-datepicker";

const mainUrl = "http://127.0.0.1:8000/api/v1";
class NewCompetition extends Component {
	constructor(props) {
		super(props);
		this.state = {
			title: "",
			location: "",
			image: null,
			start_date: "",
			end_date: "",
			loaded: 0,
			startDate: new Date(),
			endDate: new Date()
		};
		this.getCompTitle = this.getCompTitle.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}
	getCompTitle = (event) => {
		this.setState({ title: event.target.value });
		console.log(event);
	};

	fileInputHandler = () => {
		console.log("File uploaded");
	};

	getStartDateValue = (value) => {
		console.log(value);
	};

	getEndDateValue = (value) => {
		console.log(value);
	};

	handleChange(event) {
		const target = event.target;
		const value = target.type === "checkbox" ? target.checked : target.value;
		const name = target.name;

		this.setState({
			[name]: value
		});
	}

	handleSubmit = (e) => {
		e.preventDefault();
		const { title, location, start_date, end_date, image } = this.state;

		const data = new FormData();
		data.append("title", title);
		data.append("location", location);
		data.append("start_date", start_date);
		data.append("end_date", end_date);
		if (image) {
			data.append("image", image, image.name);
		}

		axios
			.post(`${mainUrl}/competition`, data, {
				onUploadProgress: (ProgressEvent) => {
					this.setState({
						loaded: ProgressEvent.loaded / ProgressEvent.total * 100
					});
				}
			})
			.then((res) => {
				console.log(res.data);
				this.props.history.push("/competition");
			});
	};

	handleSelectedFile = (e) => {
		this.setState({
			image: e.target.files[0],
			loaded: 0
		});
	};

	handleStartDateChange = (date) => {
		this.setState({
			startDate: date,
			start_date: date.toDateString()
		});
		console.log(this.state.start_date);
	};

	handleEndDateChange = (date) => {
		this.setState({
			endDate: date,
			end_date: date.toDateString()
		});
		console.log(this.state.end_date);
	};

	render() {
		return (
			<MDBContainer>
				<MDBRow>
					<MDBCol md="3" />
					<MDBCol md="6">
						<form onSubmit={this.handleSubmit}>
							<p className="h5 text-center mb-4 mt-5">Add New Competition</p>
							<div className="grey-text">
								<MDBInput
									label="Title"
									placeholder="Competition Title"
									group
									type="text"
									validate
									error="wrong"
									success="right"
									name="title"
									value={this.state.title}
									onChange={this.handleChange}
								/>
								<MDBInput
									label="Location"
									placeholder="Location"
									group
									type="text"
									validate
									error="wrong"
									success="right"
									name="location"
									value={this.state.location}
									onChange={this.handleChange}
								/>

								<div className="row md-form form-group">
									<div>
										<span className="col-4">Start Date</span>
										<DatePicker
											selected={this.state.startDate}
											onChange={this.handleStartDateChange}
											placeholderText="Click to select start date"
											className="form-control col-md-8"
											minDate={new Date()}
										/>
									</div>
									<div>
										<span className="col-md-4">End Date</span>
										<DatePicker
											selected={this.state.endDate}
											onChange={this.handleEndDateChange}
											placeholderText="Click to select end date"
											className="form-control col-md-8"
											minDate={new Date()}
										/>
									</div>
								</div>

								<div className="row md-form form-group">
									<input
										className=" ml-3"
										type="file"
										name="image"
										placeholder="Select banner image"
										onChange={this.handleSelectedFile}
									/>
									<div className="text-center"> {Math.round(this.state.loaded, 2)} %</div>
								</div>
							</div>
							<div className="text-center">
								<input className="btn btn-primary" type="submit" value="Submit" />
							</div>
						</form>
					</MDBCol>
				</MDBRow>
			</MDBContainer>
		);
	}
}

export default NewCompetition;
