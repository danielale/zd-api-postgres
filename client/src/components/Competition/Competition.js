import React, { Component } from "react";
import PropTypes from "prop-types";
import { Card, CardTitle, CardImage, CardBody, CardText, Button } from "mdbreact";

class Competition extends Component {
	render() {
		const { competition } = this.props;
		return (
			<Card style={{ width: "22rem", height: "22rem" }}>
				<CardImage className="img-fluid" src={competition.image} waves />
				<CardBody>
					<CardTitle>{competition.title}</CardTitle>
					<CardText>
						{competition.location} <br />
						{competition.start_date} -- {competition.stop_date}
					</CardText>
					<Button href="#">More</Button>
				</CardBody>
			</Card>
		);
	}
}

Competition.propTypes = {
	competition: PropTypes.object.isRequired
};

export default Competition;
