import React, { Component } from "react";
import Competition from "./Competition";
import { Col } from "mdbreact";
import axios from "axios";

class CompetitionList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			competitions: []
		};
	}

	componentWillMount() {
		axios
			.get("http://127.0.0.1:8000/api/v1/competition")
			.then((comps) => {
				console.log(comps.data);
				this.setState({ competitions: comps.data.data });
			})
			.catch((error) => {
				console.log(error);
			});
	}

	render() {
		const { competitions } = this.state;
		return (
			<div className="row">
				{competitions.map((competition) => {
					return (
						<Col key={competition.id} md="3" className="mr-3">
							<Competition competition={competition} />
						</Col>
					);
				})}
			</div>
		);
	}
}

export default CompetitionList;
