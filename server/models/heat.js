"use strict";
module.exports = (sequelize, DataTypes) => {
	const Heat = sequelize.define(
		"heat",
		{
			id: {
				type: DataTypes.UUID,
				primaryKey: true,
				defaultValue: DataTypes.UUIDV4
			},
			heat_number: { type: DataTypes.STRING, required: true },
			time: DataTypes.STRING,
			date: DataTypes.STRING
		},
		{
			paranoid: true,
			underscored: true
		}
	);
	return Heat;
};
