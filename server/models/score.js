"use strict";
module.exports = (sequelize, DataTypes) => {
	const Score = sequelize.define(
		"score",
		{
			id: {
				type: DataTypes.UUID,
				primaryKey: true,
				defaultValue: DataTypes.UUIDV4
			},
			first_position: { type: DataTypes.STRING },
			second_position: DataTypes.STRING,
			third_position: DataTypes.STRING,
			fourth_position: DataTypes.STRING,
			fifth_position: DataTypes.STRING,
			sixth_position: DataTypes.STRING,
			user: DataTypes.STRING
		},
		{
			paranoid: true,
			underscored: true
		}
	);
	return Score;
};
