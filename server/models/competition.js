"use strict";
module.exports = (sequelize, DataTypes) => {
	const Competition = sequelize.define(
		"competition",
		{
			id: {
				type: DataTypes.UUID,
				primaryKey: true,
				defaultValue: DataTypes.UUIDV4
			},
			title: { type: DataTypes.STRING, required: true },
			location: { type: DataTypes.STRING, required: true },
			image: DataTypes.STRING,
			start_date: DataTypes.STRING,
			end_date: DataTypes.STRING
		},
		{
			paranoid: true,
			underscored: true
		}
	);

	return Competition;
};
