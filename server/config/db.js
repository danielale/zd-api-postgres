"use strict";

const Sequelize = require("sequelize");
const env = require("./env");
// const sequelize = new Sequelize(env.DATABASE_NAME, env.DATABASE_USERNAME, env.DATABASE_PASSWORD, {
// 	host: env.DATABASE_HOST,
// 	port: env.DATABASE_PORT,
// 	dialect: env.DATABASE_DIALECT,
// 	define: {
// 		underscored: true
// 	}
// });

const sequelize = new Sequelize(env.DATABASE_URL);

sequelize
	.authenticate()
	.then(() => {
		console.log("Connection has been established successfully.");
	})
	.catch((err) => {
		console.error("Unable to connect to the database:", err);
	});

// Connect all the models/tables in the database to a db object,
//so everything is accessible via one object
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//Models/tables
db.competitions = require("../models/competition")(sequelize, Sequelize);
db.heats = require("../models/heat")(sequelize, Sequelize);
db.scores = require("../models/score")(sequelize, Sequelize);

//Relations
db.heats.belongsTo(db.competitions);
db.competitions.hasMany(db.heats);

db.scores.belongsTo(db.heats);
db.heats.hasMany(db.scores);

module.exports = db;
