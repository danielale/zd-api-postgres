"use strict";

const env = {
	PORT: process.env.PORT || 8089,
	DATABASE_URL:
		process.env.DATABASE_URL ||
		"postgres://hhmutjxr:1W8IDK_rc5D3iydefP9XW4C0xs4CWJcF@baasu.db.elephantsql.com:5432/hhmutjxr",
	DATABASE_NAME: process.env.DATABASE_NAME || "sequelize_blog_post",
	DATABASE_HOST: process.env.DATABASE_HOST || "localhost",
	DATABASE_USERNAME: process.env.DATABASE_USERNAME || "postgres",
	DATABASE_PASSWORD: process.env.DATABASE_PASSWORD || "",
	DATABASE_PORT: process.env.DATABASE_PORT || 5432,
	DATABASE_DIALECT: process.env.DATABASE_DIALECT || "postgres",

	NODE_ENV: process.env.NODE_ENV || "development"
};

module.exports = env;
