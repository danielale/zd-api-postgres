"use strict";
const validateCompetitionInput = require("../../validation/competition");
const validateHeatInput = require("../../validation/heat");
const validateScoreInput = require("../../validation/scores");

module.exports = (app, db) => {
	// GET all heats
	app.get("/api/v1/competition/scores", (req, res) => {
		db.scores.findAll().then((scores) => {
			res.json(scores);
		});
	});

	// GET all scores in a given heat
	app.get("/api/v1/competition/:competition_id/heats/:heat_id/score", (req, res) => {
		const { competition_id, heat_id } = req.params;
		db.heats.findAll({ heat_id: heat_id }).then((heat) => {
			if (!heat) {
				return res.status(404).json({ status: "Heat does not exist", error: error });
			}
		});
		db.scores
			.findAll({
				where: { heat_id: heat_id }
			})
			.then((scores) => {
				return res.json({ data: scores, status: "Ok" });
			});
	});

	// GET one score by id
	app.get("/api/v1/competition/heats/score/:id", (req, res) => {
		const id = req.params.id;
		db.scores
			.find({
				where: { id: id }
			})
			.then((score) => {
				if (!score) {
					return res.status(404).json({ status: "Score does not exist", error: error });
				}
				return res.json({ data: score, status: "Ok" });
			});
	});

	// GET one heat by id
	app.get("/api/v1/competition/:competition_id/heats/:heat_id/score/:user", (req, res) => {
		const { user, heat_id, competition_id } = req.params;
		db.scores
			.find({
				where: { user: user, heat_id: heat_id }
			})
			.then((score) => {
				if (!score) {
					return res.status(404).json({ status: "Score does not exist", error: error });
				}
				return res.json({ data: score, status: "Ok" });
			});
	});

	// POST single heat
	app.post("/api/v1/competition/:competition_id/heats/:heat_id/score", (req, res) => {
		// const { errors, isValid } = validateScoreInput(req.body);
		// Check Validation
		// if (!isValid) {
		// 	return res.status(400).json({ errors });
		// }
		if(!user){
			return res.status(400).json({errors: "User should not be empty"});
		}
		const {
			first_position,
			second_position,
			third_position,
			fourth_position,
			fifth_position,
			sixth_position,
			user
		} = req.body;
		const { competition_id, heat_id } = req.params;
		db.scores
			.create({
				heat_id: heat_id,
				first_position,
				second_position,
				third_position,
				fourth_position,
				fifth_position,
				sixth_position,
				user
			})
			.then((newScore) => {
				return res.json(newScore);
			});
	});

	// PATCH single heat
	app.put("/api/v1/competition/score/:id", (req, res) => {
		const id = req.params.id;
		const {
			first_position,
			second_position,
			third_position,
			fourth_position,
			fifth_position,
			sixth_position
		} = req.body;
		db.scores
			.find({
				where: { id: id }
			})
			.then((score) => {
				return score.updateAttributes({
					first_position,
					second_position,
					third_position,
					fourth_position,
					fifth_position,
					sixth_position
				});
			})
			.then((updatedScore) => {
				res.json(updatedScore);
			});
	});

	// DELETE single heat
	app.delete("/api/v1/competition/score/:id", (req, res) => {
		const id = req.params.id;
		db.scores
			.destroy({
				where: { id: id }
			})
			.then((deletedScore) => {
				res.json(deletedScore);
			});
	});

	// GET Other scores
	app.get("/api/v1/competition/:id/heats/:heatID/score", (req, res) => {
		const { id, heatID } = req.params;
		const judgeRating = {
			first_position: "001",
			second_position: "230",
			third_position: "102",
			fourth_position: "122",
			fifth_position: "422",
			sixth_position: "100"
		};
		const dancersRating = {
			first_position: "230",
			second_position: "230",
			third_position: "001",
			fourth_position: "122",
			fifth_position: "100",
			sixth_position: "422"
		};

		return res.json({ competition: id, heat: heatID, judgeRating, dancersRating });
	});
};
