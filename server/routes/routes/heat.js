"use strict";

const validateCompetitionInput = require("../../validation/competition");
const validateHeatInput = require("../../validation/heat");
const validateScoreInput = require("../../validation/scores");

module.exports = (app, db) => {
	// GET all heats
	app.get("/api/v1/competition/heats", (req, res) => {
		db.heats.findAll().then((heats) => {
			res.json(heats);
		});
	});

	// GET all heat by in a given competition
	app.get("/api/v1/competition/:competition_id/heats", (req, res) => {
		const { competition_id } = req.params;
		db.heats
			.findAll({
				where: { competition_id: competition_id }
			})
			.then((heats) => {
				return res.json({ data: heats, status: "Ok" });
			});
	});

	// GET one heat by id
	app.get("/api/v1/competition/heats/:id", (req, res) => {
		const id = req.params.id;
		db.heats
			.find({
				where: { id: id }
			})
			.then((heat) => {
				if (!heat) {
					return res.status(404).json({ status: "Heat does not exist", error: error });
				}
				return res.json({ data: heat, status: "Ok" });
			});
	});

	// GET one heat by id
	app.get("/api/v1/competition/:competition_id/heats/:id", (req, res) => {
		const id = req.params.id;
		db.heats
			.find({
				where: { id: id }
			})
			.then((heat) => {
				if (!heat) {
					return res.status(404).json({ status: "Heat does not exist", error: error });
				}
				return res.json({ data: heat, status: "Ok" });
			});
	});

	// POST single heat
	app.post("/api/v1/competition/:competition_id", (req, res) => {
		const { errors, isValid } = validateHeatInput(req.body);
		// Check Validation
		if (!isValid) {
			return res.status(400).json({ errors });
		}
		const { heat_number, date, time } = req.body;
		const { competition_id } = req.params;
		db.heats.find({ where: { competition_id: competition_id } }).then((comp) => {
			if (!comp) {
				return res.status(404).json({ status: "Competition does not exist", error: error });
			}
		});
		db.heats
			.create({
				heat_number: heat_number,
				competition_id: competition_id,
				date: date,
				time: time
			})
			.then((newHeat) => {
				return res.json({ data: newHeat, status: "Ok" });
			})
			.catch((error) => {
				return res.status(400).json({ error: error });
			});
	});

	// PATCH single heat
	app.put("/api/v1/competition/heat/:id", (req, res) => {
		const id = req.params.id;
		const { heat_number } = req.body;
		db.heats
			.find({
				where: { id: id }
			})
			.then((heat) => {
				if (!heat) {
					return res.status(404).json({ status: "Heat does not exist", error: error });
				}
				return heat.updateAttributes({ heat_number });
			})
			.then((updatedHeat) => {
				res.json(updatedHeat);
			});
	});

	// DELETE single heat
	app.delete("/api/v1/competition/heat/:id", (req, res) => {
		const id = req.params.id;
		db.heats
			.destroy({
				where: { id: id }
			})
			.then((deletedHeat) => {
				res.json(deletedHeat);
			});
	});
};
