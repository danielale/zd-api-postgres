"use strict";
const validateCompetitionInput = require("../../validation/competition");
const validateHeatInput = require("../../validation/heat");
const validateScoreInput = require("../../validation/scores");

const multer = require("multer");
const path = require("path");
const cloudinary = require("cloudinary");
const cloudinaryStorage = require("multer-storage-cloudinary");
const apiKey = require("../../config/keys").cloudinaryConfig;
// setup
cloudinary.config(apiKey);

// Init Upload
const storage = multer.diskStorage({
	destination: function(req, file, cb) {
		cb(null, "uploads/");
	},
	filename: function(req, file, cb) {
		cb(null, new Date().toISOString() + file.originalname);
	}
});

const fileFilter = (req, file, cb) => {
	// reject a file
	if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
		cb(null, true);
	} else {
		cb(null, false);
	}
};

const cloudStorage = cloudinaryStorage({
	cloudinary: cloudinary,
	folder: "dance",
	allowedFormats: [ "jpg", "png" ],
	transformation: [ { width: 500, height: 500, crop: "limit" } ]
});

const upload = multer({
	storage: cloudStorage,
	limits: {
		fileSize: 1024 * 1024 * 5
	},
	fileFilter: fileFilter
});

module.exports = (app, db) => {
	app.post("/api/v1/image-upload", (req, res) => {
		const values = Object.values(req.files);
		const promises = values.map((image) => cloudinary.uploader.upload(image.path));

		Promise.all(promises).then((results) => res.json(results));
	});

	// GET all competitions
	app.get("/api/v1/competition", (req, res) => {
		db.competitions.findAll().then((competitions) => {
			return res.send({ status: "OK", data: competitions });
		});
	});

	// GET one competition by id
	app.get("/api/v1/competition/:id", (req, res) => {
		const id = req.params.id;
		db.competitions
			.find({
				where: { id: id }
			})
			.then((competition) => {
				if (!competition) {
					return res.status(404).json({ status: "Competition does not exist", error: error });
				}
				return res.send({ status: "OK", data: competition });
			});
	});

	// POST single competition
	app.post("/api/v1/competition", (req, res) => {
		let image = "";
		const values = Object.values(req.files);
		const promises = values.map((image) => cloudinary.uploader.upload(image.path));

		Promise.all(promises).then((results) => {
			image = results[0].url;
			console.log(results);

			const { errors, isValid } = validateCompetitionInput(req.body);
			// Check Validation
			if (!isValid) {
				return res.status(400).json({ errors });
			}
			const newComp = {
				title: req.body.title,
				image: image,
				location: req.body.location,
				start_date: req.body.start_date,
				end_date: req.body.end_date
			};
			db.competitions
				.create(newComp)
				.then((newCompetition) => {
					return res.send({ status: "OK", data: newCompetition });
				})
				.catch((error) => {
					return res.status(404).json({ status: "Failed", error: error });
				});
		});

		// console.log(req.file);
		// const image = req.file.url;
	});

	// PATCH single competition
	app.put("/api/v1/competition/:id", (req, res) => {
		const id = req.params.id;
		const { image } = req.body;
		db.competitions
			.find({
				where: { id: id }
			})
			.then((competition) => {
				if (!competition) {
					return res.status(404).json({ status: "Competition does not exist", error: error });
				}
				return competition.updateAttributes({ image: image });
			})
			.then((updatedComp) => {
				res.json({ status: "Ok", data: updatedComp });
			});
	});

	// DELETE single competition
	app.delete("/api/v1/competition/:id", (req, res) => {
		const id = req.params.id;
		db.competitions
			.find({
				where: { id: id }
			})
			.then((competition) => {
				if (!competition) {
					return res.status(404).json({ status: "Competition does not exist", error: error });
				}
			});
		db.competitions
			.destroy({
				where: { id: id }
			})
			.then((deletedComp) => {
				return res.json({ data: deletedComp, status: "deleted" });
			});
	});
};
