"use strict";

const routes = [ require("./routes/competition"), require("./routes/heat"), require("./routes/score") ];

// Add access to the app and db objects to each route
module.exports = function router(app, db) {
	return routes.forEach((route) => {
		route(app, db);
	});
};
