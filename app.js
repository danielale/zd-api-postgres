const express = require("express");
const logger = require("morgan");
const bodyParser = require("body-parser");
const formData = require("express-form-data");
const cors = require("cors");
const db = require("./server/config/db.js"),
	env = require("./server/config/env"),
	router = require("./server/routes/index");
const path = require("path");
// Set up the express app
const app = express();

// Log requests to the console.
app.use(logger("dev"));

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(formData.parse());
app.use(cors());

app.use(express.static(path.join(__dirname, "client", "build")));
// Require our routes into the application.
router(app, db);

app.get("/api", (req, res) =>
	res.status(200).send({
		message: "Welcome to the ZipDance API!"
	})
);

// Setup a default catch-all route that sends back a welcome message in JSON format.
// app.get("*", (req, res) => {
// 	return res.status(404).json({ error: "Invalid Route" });
// });

app.get("*", (req, res) => {
	res.sendFile(path.join(__dirname, "client", "build", "index.html"));
});

const port = parseInt(process.env.PORT, 10) || 8000;
app.set("port", port);

//drop and resync with { force: true }
db.sequelize.sync().then(() => {
	app.listen(port, () => {
		console.log("Express listening on port:", port);
	});
});
